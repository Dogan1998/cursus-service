package nl.kzaconnected.cursus.controller;

import nl.kzaconnected.cursus.model.Dao.Cursist;
import nl.kzaconnected.cursus.model.Dto.CursusDto;
import nl.kzaconnected.cursus.service.CursusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/cursussen")
@CrossOrigin
public class CursusController {

    @Autowired
    private CursusService cursusService;

    @GetMapping("/{id}")
    public ResponseEntity<CursusDto> getCursusById(@PathVariable(value = "id") Long id) {
        CursusDto cursus = cursusService.findOne(id);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(cursus);
    }

    @GetMapping
    public ResponseEntity<List<CursusDto>> getAllCursussen() {
        return ResponseEntity.ok(cursusService.findAll());
    }

    @PostMapping
    public ResponseEntity<CursusDto> createCursus(@Valid @RequestBody CursusDto cursusDto) {
        cursusDto.setId(null);
        CursusDto cursusDtoResponse = cursusService.save(cursusDto);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(cursusDtoResponse.getId()).toUri();

        return ResponseEntity.created(location).body(cursusDtoResponse);
    }

    @PostMapping("/{id}/cursuscursisten")
    public ResponseEntity<String> createCursusCursist(@PathVariable(value = "id") Long id, @Valid @RequestBody Cursist cursist) {
        CursusDto cursus = cursusService.findOne(id);
        cursist.setId(null);

        if (cursus == null) {
            return ResponseEntity.notFound().build();
        }
        if (cursus.getMaxdeelnemers() <= cursus.getCursuscursisten().size()) {
            return ResponseEntity.badRequest().body("Het maximale aantal deelnemers voor deze cursus is bereikt.");
        }

        //opslaan cursist bij cursus
        cursus.getCursuscursisten().add(cursist);
        cursusService.save(cursus);

        return ResponseEntity.ok().body("Succesvol aangemeld voor de cursus.");
    }

    @PutMapping("/{id}")
    public ResponseEntity<CursusDto> updateCursus(@PathVariable(value = "id") Long id,@Valid @RequestBody CursusDto cursusDto) {
        cursusDto.setId(id);

        if(cursusService.findOne(id) == null) {
            return ResponseEntity.notFound().build();
        }

        CursusDto updatedCursus = cursusService.save(cursusDto);
        return ResponseEntity.ok(updatedCursus);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CursusDto> deleteCursus(@PathVariable(value = "id") Long id) {
        CursusDto cursus = cursusService.findOne(id);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }

        cursusService.delete(id);
        return ResponseEntity.ok().build();
    }
}
