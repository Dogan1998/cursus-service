package nl.kzaconnected.cursus.model.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kzaconnected.cursus.model.Dao.Cursist;
import nl.kzaconnected.cursus.model.Dao.Datum;
import nl.kzaconnected.cursus.model.Dao.Docent;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CursusDto {

    private Long id;
    private String naam;
    private String attitude;
    private String functieniveau;
    private String slagingscriterium;
    private String status;
    private int maxdeelnemers;
    private String beschrijving;
    private List<Datum> cursusdata;
    private List<Docent> cursusdocenten;
    private List<Cursist> cursuscursisten;
}